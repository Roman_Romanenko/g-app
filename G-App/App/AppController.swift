//
//  AppController.swift
//  G-App
//
//  Created by Roman Romanenko on 1/18/20.
//  Copyright © 2020 Roman Romanenko. All rights reserved.
//

import UIKit

final class AppController {
    
    // MARK: Public Property
    
    static let shared = AppController()
    
    // MARK: Private initialization
    
    private init() {}
    
    // MARK: Public Methods
    
    public func showRootScene() {
        let viewController = HomeScreenViewController()
        NavigationAssistant.setRoot(viewController)
    }
    
}

