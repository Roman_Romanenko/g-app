//
//  HomeScreenMainView.swift
//  G-App
//
//  Created by Roman Romanenko on 1/18/20.
//  Copyright © 2020 Roman Romanenko. All rights reserved.
//

import UIKit

class HSMainView: UIView {
    
    // MARK: Properties
    
    var stateLabel = HSStateLabel()
    var hsButton = HSButton()
    var alarmSection = HSAlarmView()
    var sleepTimerSection = HSSleepTimerView()
    
    // MARK: Life cycle
    
    required init?(coder aDecoder: NSCoder) {
        fatalError(aDecoder.error.debugDescription)
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        
        settings()
        configView()
    }
    
    convenience init() {
        self.init(frame: .zero)
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        
        configFrames()
    }
    
    // MARK: Methods
    
    private func settings() {
        backgroundColor = Palette.primary.backgroundColor
    }
    
    private func configView() {
        addStateLabel()
        addHsButton()
        addAlarmSection()
        addSleepTimerSection()
    }
    
    private func configFrames() {
        setStateLabelFrame()
        setHsButtonFrame()
        setAlarmSectionFrame()
        setSleepTimerSectioFrame()
    }
    
    // State label
    
    private func addStateLabel() {
        addSubview(stateLabel)
    }
    
    private func setStateLabelFrame() {
        stateLabel.anchor(
            top: topAnchor, leading: leadingAnchor, bottom: nil, trailing: trailingAnchor,
            padding: .init(
                top: Measurence.title.top,
                left: Measurence.title.left,
                bottom:Measurence.title.bottom,
                right: Measurence.title.right
            ),
            size: .init(
                width: Measurence.title.width,
                height: Measurence.title.height
            )
        )
    }
    
    // Play&Pause button
    
    private func addHsButton() {
        addSubview(hsButton)
    }
    
    private func setHsButtonFrame() {
        hsButton.anchor(
            top: nil, leading: leadingAnchor, bottom: bottomAnchor, trailing: trailingAnchor,
            padding: .init(
                top: Measurence.button.top,
                left: Measurence.button.left,
                bottom: Measurence.button.bottom,
                right: Measurence.button.right
            ),
            size: .init(
                width: Measurence.button.width,
                height: bounds.height * 0.08
            )
        )
    }
    
    // Alarm section
    
    private func addAlarmSection() {
        addSubview(alarmSection)
    }
    
    private func setAlarmSectionFrame() {
        alarmSection.anchor(
            top: nil, leading: leadingAnchor, bottom: hsButton.topAnchor, trailing: trailingAnchor,
            padding: .init(
                top: Measurence.alarmSection.top,
                left: Measurence.alarmSection.left,
                bottom: Measurence.alarmSection.bottom,
                right: Measurence.alarmSection.right
            ),
            size: .init(
                width: bounds.width,
                height: bounds.height * 0.08
            )
        )
    }
    
    // Sleep timer sectio
    
    private func addSleepTimerSection() {
        addSubview(sleepTimerSection)
    }
    
    private func setSleepTimerSectioFrame() {
        sleepTimerSection.anchor(
            top: nil, leading: leadingAnchor, bottom: alarmSection.topAnchor, trailing: trailingAnchor,
            padding: .init(
                top: Measurence.sleepTimerSection.top,
                left: Measurence.sleepTimerSection.left,
                bottom: Measurence.sleepTimerSection.bottom,
                right: Measurence.sleepTimerSection.right
            ),
            size: .init(
                width: bounds.width,
                height: bounds.height * 0.08
            )
        )
    }
    
}

// MARK: --- --- --- --- --- --- --- --- --- --- --- --- --- Measurence

extension HSMainView {
    
    private struct Measurence {
        
        struct title {
            static let width: CGFloat = 0
            static let height: CGFloat = 60
            
            static let top: CGFloat = 8
            static let right: CGFloat = 8
            static let bottom: CGFloat = 8
            static let left: CGFloat = 8
        }
        
        struct button {
            static let width: CGFloat = 0
            static let height: CGFloat = 60
            
            static let top: CGFloat = 12
            static let right: CGFloat = 12
            static let bottom: CGFloat = 12
            static let left: CGFloat = 12
        }
        
        struct alarmSection {
            static let width: CGFloat = 0
            static let height: CGFloat = 60
            
            static let top: CGFloat = 0
            static let right: CGFloat = 16
            static let bottom: CGFloat = 12
            static let left: CGFloat = 16
        }
        
        struct sleepTimerSection {
            static let width: CGFloat = 0
            static let height: CGFloat = 60
            
            static let top: CGFloat = 0
            static let right: CGFloat = 16
            static let bottom: CGFloat = 0
            static let left: CGFloat = 16
        }

    }
}
