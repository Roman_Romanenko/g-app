//
//  HomeScreenStateLabel.swift
//  G-App
//
//  Created by Roman Romanenko on 1/18/20.
//  Copyright © 2020 Roman Romanenko. All rights reserved.
//

import UIKit

class HSStateLabel: UILabel {
    
    // MARK: Properties
    
    /// This property displays current state of application's audio mode.
    /// Defualt value is: `.idle`
    var state: HSState = .idle {
        didSet {
            text = state.rawValue.capitalized
        }
    }
    
    // MARK: Life cycle
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        
        text = state.rawValue.capitalized
        textAlignment = .center
        textColor = Palette.primary.text.main
        font = UIFont(name: Font.primary.bold, size: 18)
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError(aDecoder.error.debugDescription)
    }
    
}
