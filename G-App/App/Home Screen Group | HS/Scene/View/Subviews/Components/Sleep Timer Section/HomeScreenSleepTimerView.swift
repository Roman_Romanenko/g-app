//
//  HomeScreenSleepTimerView.swift
//  G-App
//
//  Created by Roman Romanenko on 1/18/20.
//  Copyright © 2020 Roman Romanenko. All rights reserved.
//

import UIKit

class HSSleepTimerView: TwoHLabelsView {
    
    // MARK: Config
    
    override func settings() {
        super.settings()
        
        shouldAddTapGesture = true
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        
        addBorder(side: .top, thickness: 1, color: Palette.primary.border.main)
    }
    
    override func setupLeftLabel() {
        super.leftLabel.text = "Sleep Timer"
        super.leftLabel.titleSize = 16
        super.leftLabel.titleFontName = Font.primary.medium
        super.leftLabel.titleColor =  Palette.primary.text.main
    }
    
    override func setupRightLabel() {
        super.rightLabel.text = "-- min"
        super.rightLabel.textAlignment = .right
        super.rightLabel.titleSize = 16
        super.rightLabel.titleFontName = Font.primary.bold
        super.rightLabel.titleColor =  Palette.primary.text.time
    }
    
}
