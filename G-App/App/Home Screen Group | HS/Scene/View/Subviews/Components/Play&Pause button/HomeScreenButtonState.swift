//
//  HomeScreenButtonState.swift
//  G-App
//
//  Created by Roman Romanenko on 1/18/20.
//  Copyright © 2020 Roman Romanenko. All rights reserved.
//

import Foundation

enum HSButtonState: String {
    case initial
    case play
    case pause
}
