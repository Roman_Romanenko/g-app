//
//  HomeScreenButton.swift
//  G-App
//
//  Created by Roman Romanenko on 1/18/20.
//  Copyright © 2020 Roman Romanenko. All rights reserved.
//

import UIKit

class HSButton: UIButton {
    
    // MARK: Delegate
    
    weak var delegate: HSButtonDelegate?
        
    // MARK: Properties

    /// This property displays current state of application's audio mode.
    /// Defualt value is: `.initial`
    var audioState: HSButtonState = .initial {
        didSet {
            switch audioState {
            case .initial:
                isPlaying = false
                isOnPause = false
                setTitle(HSButtonState.play.rawValue.capitalized, for: .normal)
            case .play:
                isPlaying = true
                isOnPause = !isPlaying
                setTitle(HSButtonState.pause.rawValue.capitalized, for: .normal)
            case .pause:
                isOnPause = true
                isPlaying = !isOnPause
                setTitle(HSButtonState.play.rawValue.capitalized, for: .normal)
            }
        }
    }
    
    var isPlaying = false
    var isOnPause = false
    
    var titleColor: UIColor!{
        didSet {
            setTitleColor(titleColor, for: .normal)
            setTitleColor(titleColor.withAlphaComponent(0.35), for: .highlighted)
        }
    }
    
    var fontSize: CGFloat! {
        didSet {
            titleLabel?.font = titleLabel?.font.withSize(fontSize)
        }
    }
    
    var activeBgColor: UIColor!
    var notActiveBgColor: UIColor!
    
    // MARK: Life cycle
    
    required init?(coder aDecoder: NSCoder) {
        fatalError(aDecoder.error.debugDescription)
    }
    
    init(frame: CGRect, activeColor: UIColor, notActiveColor: UIColor = .lightGray, titleColor: UIColor = .white, fontSize: CGFloat) {
        super.init(frame: frame)
        
        self.activeBgColor = activeColor
        self.notActiveBgColor = notActiveColor
        self.titleColor = titleColor
        self.fontSize = fontSize
        
        settings()
    }
    
    convenience init() {
        self.init(
            frame: CGRect.zero,
            activeColor: Palette.primary.btnBackground,
            notActiveColor: .lightGray,
            fontSize: 14
        )
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        
        corner(radius: 12)
    }
    
    // Private methods
    
    private func settings() {
        audioState = .initial
        backgroundColor = activeBgColor
        titleLabel?.font = titleLabel?.font.withSize(fontSize)
        setTitleColor(titleColor, for: .normal)
        setTitleColor(titleColor.withAlphaComponent(0.35), for: .highlighted)
        addTarget(self, action: #selector(buttonAction), for: .touchUpInside)
    }
    
    // MARK: Action
    
    @objc func buttonAction(sender: HSButton!) {
        delegate?.homeScreenButtonDidTap(sender)
    }
    
}
