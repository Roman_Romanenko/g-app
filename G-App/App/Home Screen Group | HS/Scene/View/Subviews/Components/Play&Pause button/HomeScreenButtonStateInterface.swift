//
//  HomeScreenButtonStateInterface.swift
//  G-App
//
//  Created by Roman Romanenko on 1/18/20.
//  Copyright © 2020 Roman Romanenko. All rights reserved.
//

import Foundation

protocol HSButtonDelegate: AnyObject {
    func homeScreenButtonDidTap(_ sender: HSButton)
}
