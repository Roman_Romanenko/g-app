//
//  HomeScreenViewController.swift
//  G-App
//
//  Created by Roman Romanenko on 1/18/20.
//  Copyright © 2020 Roman Romanenko. All rights reserved.
//

import UIKit

class HomeScreenViewController: UIViewController {
    
    // MARK: Properties
    
    private let mainView = HSMainView(frame: UIScreen.main.bounds)
    
    // MARK: Life cycle

    override func viewDidLoad() {
        super.viewDidLoad()
        
        becomeDelegate()
    }

    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        
        attachViewToSafeArea(mainView)
    }
    
    // MARK: Methods
    
    func becomeDelegate() {
        mainView.hsButton.delegate = self
        mainView.sleepTimerSection.delegate = self
        mainView.alarmSection.delegate = self
    }
    
}

// MARK: HSButtonDelegate

extension HomeScreenViewController: HSButtonDelegate {
    
    func homeScreenButtonDidTap(_ sender: HSButton) {
        
        switch sender.audioState {
        case .initial:
            sender.audioState = .play
        case .play:
            sender.audioState = .pause
        case .pause:
            sender.audioState = .play
        }
    }
    
}

// TODO: Switch off playing the sound

extension HomeScreenViewController {
    /// ... mainView.hsButton.audioState = .initial
}

// TODO: Switch off recording

extension HomeScreenViewController {
    /// ... mainView.hsButton.audioState = .initial
}

// MARK: TwoHLabelsViewDelegate

extension HomeScreenViewController: TwoHLabelsViewDelegate {
    
    func twoHLabelsViewDidTap(sender: TwoHLabelsView) {
        
        if sender == mainView.sleepTimerSection {
            
            let vc = TimePickerViewController(for: .sleepTimer)
            vc.onDidSleepTimerSelect = { (min: String) in
                print("onDidSleepTimerSelect:", min)
            }
            present(vc, animated: true)
        }
        
        if sender == mainView.alarmSection {
            
            let vc = TimePickerViewController(for: .alarm)
            vc.onDidAlarmSelect = { (h: String, min: String, meridiems: String) in
                let time = "\(h):\(min) \(meridiems)"
                print("onDidAlarmSelect:", time)
            }
            present(vc, animated: true)
        }
        
    }
    
}
