//
//  HSState.swift
//  G-App
//
//  Created by Roman Romanenko on 1/18/20.
//  Copyright © 2020 Roman Romanenko. All rights reserved.
//

import Foundation

enum HSState: String {
    case idle
    case playing
    case recording
    case paused
    case alarm
}
