//
//  TimePickerToolButton.swift
//  G-App
//
//  Created by Roman Romanenko on 1/19/20.
//  Copyright © 2020 Roman Romanenko. All rights reserved.
//

import UIKit

class TimePickerToolButton: UIButton {
    
    // MARK: Private properties
    
    private var isActive = false
    
    // MARK: Public properties
    
    public var activeColor: UIColor!
    public var notActiveColor: UIColor!
    
    public var isUppercasedTitle: Bool! {
        didSet {
            setAppropriateSizeOfTitle()
        }
    }
    
    public var title: String! {
        didSet {
            setAppropriateSizeOfTitle()
        }
    }
    
    public var titleColor: UIColor!{
        didSet {
            setTitleColor(titleColor, for: .normal)
            setTitleColor(titleColor.withAlphaComponent(0.35), for: .highlighted)
        }
    }
    
    public var btnFontSize: CGFloat! {
        didSet {
            titleLabel?.font = titleLabel?.font.withSize(btnFontSize)
        }
    }
    
    // MARK: Life cycle
    
    required init?(coder aDecoder: NSCoder) {
        fatalError(aDecoder.error.debugDescription)
    }
    
    init(frame: CGRect, activeColor: UIColor, notActiveColor: UIColor = .lightGray, isUppercasedTitle: Bool = true, title: String = String(), titleColor: UIColor = .white, fontSize: CGFloat) {
        super.init(frame: frame)
        
        self.activeColor = activeColor
        self.notActiveColor = notActiveColor
        self.title = title
        self.titleColor = titleColor
        self.isUppercasedTitle = isUppercasedTitle
        self.btnFontSize = fontSize
        
        settings()
    }
    
    convenience init() {
        self.init(
            frame: CGRect(x: 0, y: 0, width: 0, height: 0),
            activeColor: Palette.primary.backgroundColor,
            notActiveColor: .lightGray,
            isUppercasedTitle: true,
            title: String(),
            fontSize: 12
        )
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        
        corner(radius: 20)
    }
    
    // Private methods
    
    private func settings() {
        backgroundColor = activeColor
        titleLabel?.font = titleLabel?.font.withSize(btnFontSize)
        setTitleColor(titleColor, for: .normal)
        setTitleColor(titleColor.withAlphaComponent(0.35), for: .highlighted)
        let title = isUppercasedTitle ? self.title.uppercased() : self.title
        setTitle(title, for: .normal)
    }
    
    private func setAppropriateSizeOfTitle() {
        let title = isUppercasedTitle ? self.title.uppercased() : self.title
        setTitle(title, for: .normal)
    }
    
    // Public methods
    
    public func active() {
        isActive = !isActive
        backgroundColor = activeColor
        isUserInteractionEnabled = true
    }
    
    public func notActive() {
        isActive = !isActive
        backgroundColor = notActiveColor
        isUserInteractionEnabled = false
    }
    
}

