//
//  TimePickerTollbar.swift
//  G-App
//
//  Created by Roman Romanenko on 1/19/20.
//  Copyright © 2020 Roman Romanenko. All rights reserved.
//

import UIKit

class TimePickerToolbar: UIView {
    
    // MARK: Delegate
    
    weak var delegate: TwoHLabelsViewDelegate?
    
    // MARK: Properties
    
    var title: String = "Title" {
        didSet {
            titleLabel.text = title
        }
    }
    
    let leftBtn = TimePickerToolButton()
    let titleLabel = Label()
    let rightBtn = TimePickerToolButton()
    
    // MARK: Life cycle
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        
        settings()
        configView()
    }
    
    init(frame: CGRect, titleText: String, titleFontName: String, titleSize: CGFloat) {
        super.init(frame: frame)
        
        settings()
        configView()
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        
        settings()
        configView()
    }
    
    convenience init() {
        self.init(frame: .zero)
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        
        configFrames()
    }
    
    // MARK: Methods
    
    private func configView() {
        addLeftBtn()
        addRightBtn()
        addTitleLabel()
    }
    
    private func configFrames() {
        setLeftBtnFrame()
        setRightBtnFrame()
        setTitleLabelFrame()
    }
    
    // Left btn
    
    private func addLeftBtn() {
        addSubview(leftBtn)
    }
    
    private func setLeftBtnFrame() {
        leftBtn.anchor(
            top: topAnchor, leading: leadingAnchor, bottom: bottomAnchor, trailing: nil,
            size: .init(
                width: bounds.width / 3,
                height: bounds.height
            )
        )
    }
    
    // Right btn
    
    private func addRightBtn() {
        addSubview(rightBtn)
    }
    
    private func setRightBtnFrame() {
        rightBtn.anchor(
            top: topAnchor, leading: nil, bottom: bottomAnchor, trailing: trailingAnchor,
            size: .init(
                width: bounds.width / 3,
                height: bounds.height
            )
        )
    }
    
    // Title label
    
    private func addTitleLabel() {
        addSubview(titleLabel)
    }
    
    private func setTitleLabelFrame() {
        leftBtn.layoutIfNeeded()
        rightBtn.layoutIfNeeded()
        titleLabel.anchor(
            top: topAnchor, leading: leftBtn.trailingAnchor, bottom: bottomAnchor, trailing: rightBtn.leadingAnchor
        )
    }
    
    // MARK: Overridable
    
    func settings() {
        backgroundColor = Palette.primary.backgroundColor
        setupLeftBtn()
        setupTitleLabel()
        setupRightBtn()
    }
    
    func setupLeftBtn() {
        leftBtn.title = "Cancel"
        leftBtn.titleColor = Palette.primary.text.time
    }
    
    func setupTitleLabel() {
        titleLabel.textAlignment = .center
        titleLabel.textColor = Palette.primary.text.main
    }
    
    func setupRightBtn() {
        rightBtn.title = "Done"
        rightBtn.titleColor = Palette.primary.text.time
    }
    
}

