//
//  TimePickerView.swift
//  G-App
//
//  Created by Roman Romanenko on 1/19/20.
//  Copyright © 2020 Roman Romanenko. All rights reserved.
//

import UIKit

protocol TimePickerViewSelectedMinutesDelegate: AnyObject {
    func sleepTimer(_ value: String)
}

protocol TimePickerViewSelectedTime: AnyObject {
    func setAlarmAt(_ h: String, _ min: String, _ meridiem: String)
}

class TimePickerView: UIPickerView {
    
    // MARK: Delegate
    
    weak var sleepTimerDelegate: TimePickerViewSelectedMinutesDelegate?
    weak var alarmDelegate: TimePickerViewSelectedTime?
    
    // MARK: Pprivate properties
    
    private var type: TimePickerViewType = .alarm
    
    // MARK: Properties
    
    var hours: [String]!
    var minutes: [String]!
    var meridiems: [String] = ["AM", "PM"]
    
    // MARK: Life cycle
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        self.commonSetup()
    }
    
    init(frame: CGRect = .zero, type: TimePickerViewType) {
        super.init(frame: frame)
        
        self.type = type
        self.commonSetup()
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        
        addBorder(side: .top, thickness: 1, color: Palette.primary.border.main)
    }
    
    // MARK: Methods
    
    func commonSetup() {
        backgroundColor = .white
        
        delegate = self
        dataSource = self
        
        configHoursComponent()
        configMinutesComponent()
    }
    
    func configHoursComponent() {
        var hours: [String] = []
        for h in 1...12 {
            hours.append(String(h))
        }
        self.hours = hours
    }
    
    func configMinutesComponent() {
        var minutes: [String] = []
        for min in 1...59 {
            minutes.append(String(format: "%02d", min))
        }
        
        if type == .sleepTimer { minutes.insert("off", at: 0) }
        if type == .alarm { minutes.insert("00", at: 0) }
        
        self.minutes = minutes
    }
    
    func prepareString(from collection: [String], for row: Int) -> NSAttributedString {
        let string = collection[row]
        let color = Palette.primary.text.time
        let font = UIFont(name: Font.primary.medium, size: 14)!
        let attributedString = NSAttributedString(
            string: String(string),
            attributes: [NSAttributedString.Key.foregroundColor: color, NSAttributedString.Key.font: font]
        )
        
        return attributedString
    }
    
}

// MARK: Data Source

extension TimePickerView: UIPickerViewDataSource {
    
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        
        switch type {
        case .sleepTimer:
            return 1
        case .alarm:
            return TimePickerViewComponents.allCases.count
        }
    }
    
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        
        guard type == .alarm else {
            return minutes[row]
        }
        
        switch TimePickerViewComponents(rawValue: component) {
        case .hours:
            return hours[row]
        case .minutes:
            return minutes[row]
        case .meridiem:
            return meridiems[row]
        default:
            return nil
        }
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        
        guard type == .alarm else {
            return minutes.count
        }
        
        switch TimePickerViewComponents(rawValue: component) {
        case .hours:
            return hours.count
        case .minutes:
            return minutes.count
        case .meridiem:
            return meridiems.count
        default:
            return 0
        }
    }
    
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        
        guard type == .alarm else {
            let min = minutes[self.selectedRow(inComponent: 0)]
            sleepTimerDelegate?.sleepTimer(min)
            return
        }
        
        let h = hours[self.selectedRow(inComponent: TimePickerViewComponents.hours.rawValue)]
        let min = minutes[self.selectedRow(inComponent: TimePickerViewComponents.minutes.rawValue)]
        let meridiem = meridiems[self.selectedRow(inComponent: TimePickerViewComponents.meridiem.rawValue)]
        
        alarmDelegate?.setAlarmAt(h, min, meridiem)
    }
    
}

// MARK: UIPickerViewDelegate

extension TimePickerView: UIPickerViewDelegate {
    
    func pickerView(_ pickerView: UIPickerView, attributedTitleForRow row: Int, forComponent component: Int) -> NSAttributedString? {
        
        guard type == .alarm else {
            return prepareString(from: minutes, for: row)
        }
        
        switch TimePickerViewComponents(rawValue: component) {
        case .hours:
            return prepareString(from: hours, for: row)
        case .minutes:
            return prepareString(from: minutes, for: row)
        case .meridiem:
            return prepareString(from: meridiems, for: row)
        default:
            return NSAttributedString()
        }
    }
    
}
