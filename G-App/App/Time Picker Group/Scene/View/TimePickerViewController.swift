//
//  TimePickerViewController.swift
//  G-App
//
//  Created by Roman Romanenko on 1/19/20.
//  Copyright © 2020 Roman Romanenko. All rights reserved.
//

import UIKit

class TimePickerViewController: UIViewController {
    
    // MARK: Properties
    
    var onDidSleepTimerSelect: (( _ min: String) -> Void)?
    var onDidAlarmSelect: ((_ hours: String, _ minutes: String, _ meridiems: String) -> Void)?
    
    // MARK: Private Pproperties
    
    private let type: TimePickerViewType
    private let timePickerToolbar = TimePickerToolbar()
    private let timePickerView: TimePickerView
    
    private var sleepTimerValue = "off"
    private var alarmValue: (h: String, min: String, meridiem: String) = ("1", "00", "AM")

    // MARK: Life cycle
    
    required init?(coder aDecoder: NSCoder) {
        fatalError(aDecoder.error.debugDescription)
    }
    
    init(for type: TimePickerViewType) {
        self.type = type
        timePickerView = TimePickerView(type: type)
        super.init(nibName: nil, bundle: nil)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        view.backgroundColor = .clear
        view.addSubview(timePickerView)
        addTimePickerToolbar()

        becomeDelegates()
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        
        timePickerView.anchor(
            top: nil, leading: view.leadingAnchor, bottom: view.bottomAnchor, trailing: view.trailingAnchor,
            size: .init(
                width: view.bounds.width,
                height: Measurence.alarmPicker.height
            )
        )
        
        setTimePickerToolbarFrame()
    }
    
    // MARK: Methods
    
    func becomeDelegates() {
        switch type {
        case .sleepTimer:
            timePickerView.sleepTimerDelegate = self
        case .alarm:
            timePickerView.alarmDelegate = self
        }
    }
    
    private func addTimePickerToolbar() {
        
        switch type {
        case .sleepTimer:
            timePickerToolbar.rightBtn.addTarget(self, action: #selector(sleepTimerDoneAction), for: .touchUpInside)
            timePickerToolbar.title = "Sleep Timer"
        case .alarm:
            timePickerToolbar.rightBtn.addTarget(self, action: #selector(alarmDoneAction), for: .touchUpInside)
            timePickerToolbar.title = "Alarm"
        }
        
        timePickerToolbar.leftBtn.addTarget(self, action: #selector(cancelAction), for: .touchUpInside)
        
        view.addSubview(timePickerToolbar)
    }
    
    private func setTimePickerToolbarFrame() {
        timePickerToolbar.anchor(
            top: nil, leading: timePickerView.leadingAnchor, bottom: timePickerView.topAnchor, trailing: timePickerView.trailingAnchor,
            size: .init(
                width: view.bounds.width,
                height: 44
            )
        )
    }
    
    // MARK: Actions
    
    @objc func cancelAction(sender: TimePickerToolButton!) {
        dismiss(animated: true)
    }
        
    @objc func sleepTimerDoneAction(sender: TimePickerToolButton!) {
        
        guard let block = onDidSleepTimerSelect else { return }
        block(sleepTimerValue)
        dismiss(animated: true)
    }
        
    @objc func alarmDoneAction(sender: TimePickerToolButton!) {
        
        guard let block = onDidAlarmSelect else { return }
        block(alarmValue.h, alarmValue.min, alarmValue.meridiem)
        dismiss(animated: true)
    }
    
}

// MARK: TimePickerViewSelectedMinutesDelegate

extension TimePickerViewController: TimePickerViewSelectedMinutesDelegate {
    
    func sleepTimer(_ value: String) {
        sleepTimerValue = value
    }
    
}

// MARK: TimePickerViewSelectedTime

extension TimePickerViewController: TimePickerViewSelectedTime {
    
    func setAlarmAt(_ h: String, _ min: String, _ meridiem: String) {
        alarmValue = (h: h, min: min, meridiem: meridiem)
    }
    
}

// MARK: --- --- --- --- --- --- --- --- --- --- --- --- --- Measurence

extension TimePickerViewController {
    
    private struct Measurence {
        
        struct alarmPicker {
            static let width: CGFloat = 0
            static let height: CGFloat = 200
            
            static let top: CGFloat = 0
            static let right: CGFloat = 0
            static let bottom: CGFloat = 20
            static let left: CGFloat = 0
        }

    }
}


