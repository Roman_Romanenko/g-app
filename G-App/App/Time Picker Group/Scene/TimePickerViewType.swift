
//
//  TimePickerViewType.swift
//  G-App
//
//  Created by Roman Romanenko on 1/19/20.
//  Copyright © 2020 Roman Romanenko. All rights reserved.
//

import Foundation

enum TimePickerViewType {
    case sleepTimer
    case alarm
}
