//
//  TwoHLabelsView.swift
//  G-App
//
//  Created by Roman Romanenko on 1/18/20.
//  Copyright © 2020 Roman Romanenko. All rights reserved.
//

import UIKit

protocol TwoHLabelsViewDelegate: AnyObject {
    func twoHLabelsViewDidTap(sender: TwoHLabelsView)
}

class TwoHLabelsView: UIView {
    
    // MARK: Delegate
    
    weak var delegate: TwoHLabelsViewDelegate?
    
    // MARK: Properties
    
    let leftLabel = Label()
    let rightLabel = Label()
    
    // MARK: Life cycle
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        
        settings()
        configView()
    }
    
    init(frame: CGRect, titleText: String, titleFontName: String, titleSize: CGFloat) {
        super.init(frame: frame)
        
        settings()
        configView()
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        
        settings()
        configView()
    }
    
    convenience init() {
        self.init(frame: .zero)
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        
        configFrames()
    }
    
    // MARK: Methods
    
    private func configView() {
        addLeftLabel()
        addRightLabel()
    }
    
    private func configFrames() {
        setLeftLabelFrame()
        setRightLabelFrame()
    }
    
    // Left label
    
    private func addLeftLabel() {
        addSubview(leftLabel)
    }
    
    private func setLeftLabelFrame() {
        let width: CGFloat = bounds.width * 0.5
        let height: CGFloat = bounds.height * 0.9
        let x: CGFloat = 0
        let y: CGFloat = bounds.height / 2 - height / 2
        leftLabel.frame = CGRect(x: x, y: y, width: width, height: height)
    }
    
    // Right label
    
    private func addRightLabel() {
        addSubview(rightLabel)
    }
    
    private func setRightLabelFrame() {
        let width: CGFloat = bounds.width * 0.5
        let height: CGFloat = bounds.height * 0.9
        let x: CGFloat = leftLabel.frame.maxX
        let y: CGFloat = bounds.height / 2 - height / 2
        rightLabel.frame = CGRect(x: x, y: y, width: width, height: height)
    }
    
    // MARK: Overridable
    
    func settings() {
        backgroundColor = .clear
        setupLeftLabel()
        setupRightLabel()
    }
    
    func setupLeftLabel() {}
    func setupRightLabel() {}
    
    var shouldAddTapGesture = false {
        didSet {
            guard shouldAddTapGesture else { return }
            let tap = UITapGestureRecognizer(target: self, action: #selector(tapAction))
            isUserInteractionEnabled = true
            addGestureRecognizer(tap)
        }
    }
    
    // MARK: Action
    
    @objc func tapAction(sender: UITapGestureRecognizer) {
        delegate?.twoHLabelsViewDidTap(sender: self)
    }
    
}


