//
//  Label.swift
//  G-App
//
//  Created by Roman Romanenko on 1/18/20.
//  Copyright © 2020 Roman Romanenko. All rights reserved.
//

import UIKit

class Label: UILabel {

    // MARK: Properties
    
    var titleText = String() {
        didSet {
            text = titleText
        }
    }
    
    var titleColor: UIColor = .black {
        didSet {
            textColor = titleColor
        }
    }
    
    var titleAlignment: NSTextAlignment = .left {
        didSet {
            textAlignment = titleAlignment
        }
    }
    
    var titleFontName = String() {
        didSet {
            font = UIFont(name: titleFontName, size: font?.pointSize ?? UIFont.labelFontSize)
        }
    }
    
    var titleSize: CGFloat = 0 {
        didSet {
            font = UIFont(name: font.familyName, size: titleSize)
        }
    }
    
}
