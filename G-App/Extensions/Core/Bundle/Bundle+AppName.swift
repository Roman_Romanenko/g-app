//
//  Bundle+AppName.swift
//  G-App
//
//  Created by Roman Romanenko on 1/18/20.
//  Copyright © 2020 Roman Romanenko. All rights reserved.
//

import Foundation

let appName = Bundle.main.appName

extension Bundle {
    
    var appName: String {
        guard let bundle = Bundle.main.infoDictionary else { return String() }
        guard let appName = bundle[kCFBundleNameKey as String] as? String else { return String() }
        return appName
    }
    
}
