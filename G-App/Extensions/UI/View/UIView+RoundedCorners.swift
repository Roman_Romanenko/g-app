//
//  UIView+RoundedCorners.swift
//  G-App
//
//  Created by Roman Romanenko on 1/18/20.
//  Copyright © 2020 Roman Romanenko. All rights reserved.
//

import UIKit

extension UIView {
    
    func corner(radius: CGFloat, clipsToBounds: Bool = true) {
        self.layer.cornerRadius = radius
        self.clipsToBounds = clipsToBounds
    }
    
}

