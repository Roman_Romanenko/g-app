//
//  UIViewController+AttachViewToSafeArea.swift
//  G-App
//
//  Created by Roman Romanenko on 1/18/20.
//  Copyright © 2020 Roman Romanenko. All rights reserved.
//

import UIKit

extension UIViewController {
    
    func attachViewToSafeArea(_ attachingView: UIView, sameColorForView: Bool = true) {
        attachingView.translatesAutoresizingMaskIntoConstraints = false
        if sameColorForView { view.backgroundColor = attachingView.backgroundColor }
        view.addSubview(attachingView)
        
        let constraints: [NSLayoutConstraint] = [
            attachingView.topAnchor.constraint(equalTo: view.safeTopAnchor),
            attachingView.leadingAnchor.constraint(equalTo: view.leadingAnchor),
            attachingView.trailingAnchor.constraint(equalTo: view.trailingAnchor),
            attachingView.bottomAnchor.constraint(equalTo: view.safeBottomAnchor)
        ]
        
        view.removeConstraints(constraints)
        attachingView.removeConstraints(constraints)
        NSLayoutConstraint.activate(constraints)
    }
    
}
