//
//  Palette.swift
//  G-App
//
//  Created by Roman Romanenko on 1/18/20.
//  Copyright © 2020 Roman Romanenko. All rights reserved.
//

import UIKit

public struct Palette {
    
    struct primary {
        static let backgroundColor = UIColor.white
        static let btnBackground = UIColor(red: 2/255, green: 126/255, blue: 251/255, alpha: 1) // blue
        
        struct text {
            static let main = UIColor(red: 77/255, green: 77/255, blue: 77/255, alpha: 1) // darkGray
            static let time = Palette.primary.btnBackground // blue
            static let white = UIColor.white
        }
        
        struct border {
            static let main = UIColor(red: 220/255, green: 220/255, blue: 220/255, alpha: 1) // lightGrau
        }
        
    }
    
}
