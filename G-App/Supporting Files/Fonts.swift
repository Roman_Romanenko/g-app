//
//  Fonts.swift
//  G-App
//
//  Created by Roman Romanenko on 1/18/20.
//  Copyright © 2020 Roman Romanenko. All rights reserved.
//

import UIKit

public struct Font {
    
    struct primary {
        private static let primary = "HelveticaNeue"
        static let light = "\(primary)-Light"
        static let medium = "\(primary)-Medium"
        static let bold = "\(primary)-Bold"
    }
    
}

